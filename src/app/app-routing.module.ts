import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


const routes: Routes = [
    {
        path: 'home',
        loadChildren: './home/home.module#HomeModule',
    }, {
        path: 'service',
        loadChildren: './service/service.module#ServiceModule',
    }, {
        path: 'portfolio',
        loadChildren: './portfolio/portfolio.module#PortfolioModule',
    }, {
        path: 'about-us',
        loadChildren: './about-us/about-us.module#AboutUsModule',
    },
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', redirectTo: ''},
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}