import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Injector, NgModule} from '@angular/core';
import { createCustomElement } from '@angular/elements';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import { FooterComponent } from './common/footer/footer.component';
import {HeaderComponent} from './common/header/header.component';
import { SliderComponent } from './common/slider/slider.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        SliderComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
    ],
})

export class AppModule { }