
import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Result} from './result';
import {SliderService} from './slider.service';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {animate, state, style, transition, trigger} from '@angular/animations';
@Component({
    selector: 'app-slider',
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.sass'],
    animations: [
        trigger('thumbState', [
            state('inactive', style({
                opacity: 0,
                // transform: 'scale(0.5)',
                transform: 'translateZ( -50px) rotateY(   0deg)'
            })),
            state('active', style({
                opacity: 1,
                // transform: 'scale(1)',
                transform: 'translateZ(-100px) rotateX( 0deg)'
            })),
            // cubic-bezier from http://easings.net/
            transition('inactive => active',
                animate('0.5s ease-out', style({ left: '-100%' })),

                // animate('500ms cubic-bezier(0.785, 0.135, 0.15, 0.86)')
            ),
            transition('active => inactive', animate('500ms cubic-bezier(0.785, 0.135, 0.15, 0.86)'))
        ])
    ],
    encapsulation: ViewEncapsulation.Native,
    providers: [ SliderService , HttpClient],
})
export class SliderComponent implements OnInit {
    sliderArray: Result[];
    transform: number;
    selectedIndex = 0;

    constructor(private data: SliderService) {
        this.selectedIndex = 0;
        this.transform = 100;
    }

    ngOnInit() {
        this.data.getData().subscribe((result: Result[]) => {
            this.sliderArray = result;
        });
    }

    selected(x) {
        console.log(x,'select');
        this.downSelected(x);
        this.selectedIndex = x;
    }

    keySelected(x) {
        console.log(x,'key');
        this.downSelected(x);
        this.selectedIndex = x;
    }

    downSelected(i) {
        console.log(i);
        this.transform =  100 - (i) * 50;
        this.selectedIndex = this.selectedIndex + 1;
        if (this.selectedIndex > 4) {
            this.selectedIndex = 0;
        }
    }


}