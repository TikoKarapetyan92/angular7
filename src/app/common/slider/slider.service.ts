import { Injectable } from '@angular/core';
import {Observable, of as observableOf} from 'rxjs';
import {Result} from './result';

@Injectable()
export class SliderService {

    constructor() {
    }

   private sliderArray: Result[] = [
        {img: 'http://bloquo.cc/img/works/2.jpg', alt: '',  text: 'test'},
        {img: 'http://bloquo.cc/img/works/3.jpg', alt: '', text: 'test'},
        {img: 'http://bloquo.cc/img/works/4.jpg', alt: '',  text: 'test'},
        {img: 'http://bloquo.cc/img/works/5.jpg', alt: '', text: 'test'}
    ];

    getData(): Observable<Result[]> {
        return observableOf(this.sliderArray);
    }
}